package com.elavon.jtutorial;

public class ArrayTraversal {

	public static double getTotal(int[] number) {
		double sum = 0;

		for (int i = 0; i < number.length; i++) {
			sum += number[i];
		}

		return sum;
	}

	public static double getAverage(int[] number) {
		double ave = 0;
		
		ave = getTotal(number) / number.length;
		return ave;
	}

	public static void main(String[] args) {
		int[] scores = { 100, 99 };

		System.out.println("sum = " + getTotal(scores));
		
		System.out.println("average = " + getAverage(scores));

		// int[] number = new int[10];
		//
		// // for loop to initialize every element
		// for (int i = 0; i < number.length; i++) {
		// number[i] = i;
		// }
		//
		// for (int i = 0; i < number.length; i++) {
		// System.out.println("number["+ i+ "] = " + number[i]);
		// }

	}

}
