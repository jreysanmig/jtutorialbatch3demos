package com.elavon.jtutorial;

public class MultiDimArrayDemo {

	public static void main(String[] args) {
		int[][] room = new int[3][];
		int[] floor0 = new int[3]; 
		int[] floor1 = {100,101,102,103};
		int[] floor2 = {200,201};
		                
		room[0] = floor0;
		room[1] = floor1;
		room[2] = floor2;
		        
		System.out.println(room[0][2]);
		System.out.println(room[2][0]);
		System.out.println(room[1][2] + "" + room[2][1]);

		


	}

}
