package com.elavon.jtutorial;

import java.util.ArrayList;
import java.util.List;

class Pokemon { public void attack() { System.out.println("Normal attack");} }

class WaterPokemon extends Pokemon { public void attack() { System.out.println("Bubble attack");} }
class FirePokemon extends Pokemon{ public void attack() { System.out.println("Burn attack");} }
class GrassPokemon extends Pokemon{ public void attack() { System.out.println("Cut attack");} }
class ElectricPokemon extends Pokemon{ public void attack() { System.out.println("Thunderbolt attack");} }

public class PolyListDemo {

	public static void main(String[] args) {
		Pokemon p = new WaterPokemon();
		Pokemon p2 = new FirePokemon();
		Pokemon p3 = new GrassPokemon();
		
		List<Pokemon> pokeList = new ArrayList<>();
		pokeList.add(p);
		pokeList.add(p2);
		pokeList.add(p3);
		pokeList.add(new ElectricPokemon());
		
		for(Pokemon pokemon : pokeList) {
			pokemon.attack();
		}

	}

}
