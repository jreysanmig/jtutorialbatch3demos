package com.elavon.jtutorial;

public class CommandLineDemo {

	public static void main(String[] args) {
		
		System.out.println("Arguments:");
		for(String s : args) {
			System.out.println("> " + s);
		}

	}

}
